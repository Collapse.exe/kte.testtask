package kte.testTask.patientdoctorappointment.repository;

import kte.testTask.patientdoctorappointment.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {
    List<Ticket> findAllBySelectedDayAndPatientIdIsNull(LocalDate day);
    List<Ticket> findAllByPatientId_Id(Long patientId_id);
    List<Ticket> findAllByPatientId_Uuid(UUID uuid);
}
