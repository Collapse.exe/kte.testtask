package kte.testTask.patientdoctorappointment.service;

import kte.testTask.patientdoctorappointment.exceptions.ModelNotFoundException;
import kte.testTask.patientdoctorappointment.model.*;
import kte.testTask.patientdoctorappointment.repository.DoctorRepository;
import kte.testTask.patientdoctorappointment.repository.PatientRepository;
import kte.testTask.patientdoctorappointment.repository.TicketRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public class TicketService {
    @Autowired
    private TicketRepository ticketRepository;
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private PatientRepository patientRepository;

    public List<Ticket> getFreeSlotsList(Long id,String uuid, int year, int month, int day){
        LocalDate selectedDay = LocalDate.of(year,month,day);
        UUID docUuid = UUID.fromString(uuid);
        CompositeId doctorId = new CompositeId(id, docUuid);
        Doctor doctor = doctorRepository.findById(doctorId).orElseThrow(() -> new ModelNotFoundException(doctorId.toString()));
        List<Ticket> doctorsTickets = doctor.getTicketId();
        List<Ticket> dailyTickets = ticketRepository.findAllBySelectedDayAndPatientIdIsNull(selectedDay);
        for (Ticket doctorsTicket : doctorsTickets) {
            dailyTickets.removeIf(dailyTicket -> doctorsTicket.getVisitStart().equals(dailyTicket.getVisitStart()));
        }
        return dailyTickets;
    }
    public List<Ticket> getAllMyTickets(String id){
        if (StringUtils.isNumeric(id)){
            return ticketRepository.findAllByPatientId_Id(Long.valueOf(id));
        }
        else return ticketRepository.findAllByPatientId_Uuid(UUID.fromString(id));
    }
    public void appointmentMake(AppointmentInfo info){
        Ticket ticket = ticketRepository.findById(info.getTicketId()).orElseThrow(() -> new ModelNotFoundException(info.getTicketId().toString()));
        CompositeId docId = new CompositeId(info.getDocId(), info.getDocUUID());
        CompositeId patId = new CompositeId(info.getPatId(), info.getPatUUID());
        Patient patient = patientRepository.findById(patId).orElseThrow(() -> new ModelNotFoundException(info.getPatId().toString()));
        Doctor doctor = doctorRepository.findById(docId).orElseThrow(() -> new ModelNotFoundException(info.getDocId().toString()));
        ticket.setPatientId(patient);
        ticket.setDoctorId(doctor);
        ticketRepository.save(ticket);
    }
    /*public List<Ticket> getAllTickets(){
        return ticketRepository.findAll();
    }*/
}
