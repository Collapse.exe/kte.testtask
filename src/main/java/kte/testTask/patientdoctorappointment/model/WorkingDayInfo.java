package kte.testTask.patientdoctorappointment.model;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class WorkingDayInfo {

    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Duration duration;
    private short multiplier;
    private LocalTime startOfWorkingDay;
    private LocalTime endOfWorkingDay;


    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public short getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(short multiplier) {
        this.multiplier = multiplier;
    }

    public LocalTime getStartOfWorkingDay() {
        return startOfWorkingDay;
    }

    public void setStartOfWorkingDay(LocalTime startOfWorkingDay) {
        this.startOfWorkingDay = startOfWorkingDay;
    }

    public LocalTime getEndOfWorkingDay() {
        return endOfWorkingDay;
    }

    public void setEndOfWorkingDay(LocalTime endOfWorkingDay) {
        this.endOfWorkingDay = endOfWorkingDay;
    }
}
