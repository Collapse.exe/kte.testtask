package kte.testTask.patientdoctorappointment.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class CompositeId implements Serializable {
    private Long id;
    private UUID uuid;

    public CompositeId() {
    }

    public CompositeId(Long id, UUID uuid) {
        this.id = id;
        this.uuid = uuid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CompositeId compositeId)) return false;
        return id.equals(compositeId.id) && uuid.equals(compositeId.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, uuid);
    }

    @Override
    public String toString() {
        return "CompositeId{" +
                "id=" + id +
                ", uuid=" + uuid +
                '}';
    }
}
